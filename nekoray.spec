%define         qhotkeyver 1.5.0
Name:           nekoray
Version:        4.0.1
Release:        1%{?dist}
Summary:        Qt based cross-platform GUI proxy configuration manager (backend: sing-box)

License:        GPL-3.0-only
URL:            https://matsuridayo.github.io/
Source0:        nekoray-%{version}.tar.gz
Source1:        https://github.com/Skycoder42/QHotkey/archive/refs/tags/%{qhotkeyver}.tar.gz
Source2:        nekobox.metainfo.xml
Source3:        nekobox.desktop

Patch0:         0000-fix-cmake-appdata.patch
Patch1:         0001-remove-ads.patch
Patch2:         0002-remove-update.patch
Patch3:         0003-fix-cmake-protobuf.patch

BuildRequires:  gcc-c++
BuildRequires:  cmake
BuildRequires:  qt6-linguist
BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qtsvg-devel
BuildRequires:  qt6-qttools-devel
BuildRequires:  protobuf-devel
BuildRequires:  yaml-cpp-devel
BuildRequires:  zxing-cpp-devel
Requires:       nekoray-core

%description
Qt based cross-platform GUI proxy configuration manager (backend: sing-box)
Support Windows / Linux out of the box now.

%prep
%setup -q
%autopatch -p1

%setup -a 1 -D -T
rm -r 3rdparty/QHotkey
mv QHotkey-%{qhotkeyver} 3rdparty/QHotkey

%build
%cmake -DNKR_PACKAGE=true -DNKR_DISABLE_LIBS=true -DQT_VERSION_MAJOR=6
%cmake_build

%install
install -D -m 755 %{_builddir}/nekoray-%{version}/redhat-linux-build/nekobox %{buildroot}%{_bindir}/nekobox
install -D -m 644 %{_builddir}/nekoray-%{version}/res/public/nekobox.png %{buildroot}%{_datadir}/icons/hicolor/256x256/apps/nekobox.png
#install -D -m 644 %{_builddir}/nekoray-%{version}/nekobox.desktop %{buildroot}%{_datadir}/applications/nekobox.desktop
#install -D -m 644 %{_builddir}/nekoray-%{version}/nekobox.metainfo.xml %{buildroot}%{_metainfodir}/nekobox.metainfo.xml
install -D -m 644 %{SOURCE2} %{buildroot}%{_metainfodir}/nekobox.metainfo.xml
install -D -m 644 %{SOURCE3} %{buildroot}%{_datadir}/applications/nekobox.desktop

%files
%license LICENSE
%doc README.md
%{_bindir}/nekobox
%{_datadir}/icons/hicolor/256x256/apps/nekobox.png
%{_datadir}/applications/nekobox.desktop
%{_metainfodir}/nekobox.metainfo.xml



%changelog
* Sat Dec 28 2024 sen liu <18370222-liusen373@users.noreply.gitlab.com> - 4.0.1-1
- Nekoray v4.0.1

* Sat Aug 31 2024 sen liu <18370222-liusen373@users.noreply.gitlab.com> - 4.0.beta3-1
- Nekoray latest: 2a177256ce7fe66c6e247985810a930c6ebc09e7

* Fri May 24 2024 sen liu <18370222-liusen373@users.noreply.gitlab.com> - 3.26-1
- Nekoray v3.26
